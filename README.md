# README #

zabbix-veeam template discovers Veeam backup jobs and provides information to Zabbix:

 - current job run status (running/stopped)
 - last job result (success/failed/warning)
 - last job bytes, duration, endtime

Corresponding triggers are provided:

 - last job result not OK
 - no info about job for 48 hours
 
All logic is in a single PowerShell script. Zabbix calls it for discovery and fetching jobs information.

Tested on Veeam Backup & Replication 8.0.0.2030 and Zabbix 3.4.3.

## Requirements ##

1. PowerShell on Veeam host (I believe you have it)
2. zabbix_sender available on Veeam host
3. Veeam host must be present in your Zabbix (no matter if proxy is used)

## Installation ##

1. Import file ```zbx_template_veeam.xml``` to Zabbix templates.
2. Copy ```zabbix-veeam-job-info.ps1``` to ```%WINDIR%\System32``` of Veeam Backup & Replication host.
3. Edit the file ```zabbix-veeam-job-info.ps1```:

 * ```$zabbix_sender``` = path to zabbix_sender
 * ```$hostname``` = host name of Veeam host as it is in Zabbix
 * ```$zabbix_server``` = hostname or IP of your Zabbix server or proxy
 * ```$unixtime_offset``` = ...deprecated...
 
4. Assign template ```Template Veeam Jobs``` to your Veeam host.