param([switch]$discovery, [string]$jobname, [switch]$status)

#Set-PSDebug -Trace 1


$zabbix_sender = 'C:\distrib\zabbix_agentd\zabbix_sender.exe'
$hostname = $env:computername
$zabbix_server = "zabbix.bojole.local"
$unixtime_offset = -10800	# offset from UTC in seconds. -10800 is for Moscow


Add-PSSnapin VeeamPSSnapin

if ($discovery) {
	$jobs_json = @{
		'data' = @(
			Get-VBRJob | where { $_.JobType -eq "Backup" } | select name | % {
				@{
			 		'{#JOBNAME}' = $_.Name
				}
			}	 	
		)
	} | ConvertTo-Json

	write-host $jobs_json

} else {
	if ($status) {
		$job = Get-VBRJob | where { $_.Name -eq $jobname }
		$jobstatus = switch ($job.IsRunning) {
			false 	{ "0" }
			true 	{ "1" }
			default	{ "99" }
		}
		
		write-host $jobstatus
		
	} else {
		$jobinfo = Get-VBRBackupSession | where { $_.OrigJobName -eq $jobname } | sort EndTime -Descending | select origjobname,endtime,result,workdetails,sessioninfo -first 1

		$endtime_unix = (New-TimeSpan -Start (Get-Date -Date "01/01/1970") -End $($jobinfo.EndTime)).TotalSeconds + $unixtime_offset

		write-host "Job: $($jobinfo.OrigJobName); EndTime: $($jobinfo.EndTime); Result: $($jobinfo.Result)"     
	
		$result_code = switch ($jobinfo.Result) {
			"Failed" 	{ "0" }
			"Warning" 	{ "1" }
			"Success" 	{ "2" }
			default		{ "99" }
		}

		$param_key = '-k veeam.backup.job.last.result[\"' + $jobname + '\"]'
		Start-Process -FilePath $zabbix_sender -ArgumentList "-z $zabbix_server", "-s $hostname", $param_key, "-o $result_code"
		
		$param_key = '-k veeam.backup.job.last.endtime[\"' + $jobname + '\"]'
		Start-Process -FilePath $zabbix_sender -ArgumentList "-z $zabbix_server", "-s $hostname", $param_key, "-o $endtime_unix"

		$param_key = '-k veeam.backup.job.last.duration[\"' + $jobname + '\"]'
		Start-Process -FilePath $zabbix_sender -ArgumentList "-z $zabbix_server", "-s $hostname", $param_key, "-o $($jobinfo.WorkDetails.WorkDuration.TotalSeconds)"

		$param_key = '-k veeam.backup.job.last.bytes[\"' + $jobname + '\"]'
		Start-Process -FilePath $zabbix_sender -ArgumentList "-z $zabbix_server", "-s $hostname", $param_key, "-o $($jobinfo.SessionInfo.BackedUpSize)"
	}
}
